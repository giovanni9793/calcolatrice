#ifndef CARTE_EROI_H
#define CARTE_EROI_H
#include <string>
#include <iostream>

using namespace std;

int plus2number(const string&);

class supporter;
class debuffer;
class eroi;

class comuni
{
public:
    comuni() :nome("nessuno"), vita(0), energia(0), usato(false) {}
    comuni(string n, int v, int e) : nome(n), vita(v), energia(e), usato(false) {}
    virtual comuni operator=(const comuni&);
    virtual comuni operator+(const comuni&) const;
    virtual comuni operator-(const comuni&) const;
    comuni operator*(const supporter&) const;
    comuni operator/(const supporter&) const;
    comuni operator*(const debuffer&) const;
    comuni operator/(const debuffer&) const;
    comuni operator*(const eroi&) const;
    comuni operator/(const eroi&) const;

    string Nome() const;
    int Vita() const;
    int Energia() const;
    bool Usato() const;
protected:
    string nome;
    int vita;
    int energia;
    bool usato;
};

ostream& operator<<(ostream& os, const comuni& c);

class supporter : public comuni {
public:
    supporter() : supporto("nessuno"), eff_supporto(0) {nome= "nessuno"; vita=0; energia=0; usato=false;}
    supporter(comuni c) : supporto("nessuno"), eff_supporto(0) {nome=c.Nome(); vita=c.Vita(); energia=c.Energia(); usato=false;}
    supporter(string n, int v, int e, string s, int es) : supporto(s), eff_supporto(es) {nome=n; vita=v; energia=e; usato=false;}
    virtual supporter operator=(const supporter&);
    virtual supporter operator+(const supporter&) const;
    virtual supporter operator-(const supporter&) const;
    virtual supporter operator*(const supporter&) const;
    virtual supporter operator/(const supporter&) const;

    virtual supporter operator+(const debuffer&) const;
    virtual supporter operator-(const debuffer&) const;
    virtual supporter operator*(const debuffer&) const;
    virtual supporter operator/(const debuffer&) const;

    virtual supporter operator+(const eroi&) const;
    virtual supporter operator-(const eroi&) const;
    virtual supporter operator*(const eroi&) const;
    virtual supporter operator/(const eroi&) const;

    string Supporto() const;
    int Eff_Supporto() const;
private:
    string supporto;
    int eff_supporto;
};

ostream& operator<<(ostream& os, const supporter& c);

class debuffer : public comuni {
public:
    debuffer() : debuff("nessuno"), eff_debuff(0) {nome="nessuno"; vita=0; energia=0; usato=false;}
    debuffer(string n, int v, int e, string d, int ed) : debuff(d), eff_debuff(ed) {nome=n; vita=v; energia=e; usato=false;}
    virtual debuffer operator=(const debuffer&);
    virtual debuffer operator+(const debuffer&) const;
    virtual debuffer operator-(const debuffer&) const;
    virtual debuffer operator*(const debuffer&) const;
    virtual debuffer operator/(const debuffer&) const;

    virtual debuffer operator+(const supporter&) const;
    virtual debuffer operator-(const supporter&) const;
    virtual debuffer operator*(const supporter&) const;
    virtual debuffer operator/(const supporter&) const;

    virtual debuffer operator+(const eroi&) const;
    virtual debuffer operator-(const eroi&) const;
    virtual debuffer operator*(const eroi&) const;
    virtual debuffer operator/(const eroi&) const;

    string Debuff() const;
    int Eff_Debuff() const;
private:
    string debuff;
    int eff_debuff;
};

ostream& operator<<(ostream& os, const debuffer& c);

class eroi : public comuni {
public:
    eroi() : supporto("nessuno"), eff_supporto(0), debuff("nessuno"), eff_debuff(0) {nome="nessuno"; vita=0; energia=0; usato=false;}
    eroi(string n, int v, int e, string d, int ed, string s, int es) : supporto(s), eff_supporto(es), debuff(d), eff_debuff(ed) {nome=n; vita=v; energia=e; usato=false;}
    virtual eroi operator=(const eroi&);
    virtual eroi operator+(const eroi&) const;
    virtual eroi operator-(const eroi&) const;
    virtual eroi operator*(const eroi&) const;
    virtual eroi operator/(const eroi&) const;

    virtual eroi operator+(const supporter&) const;
    virtual eroi operator-(const supporter&) const;
    virtual eroi operator*(const supporter&) const;
    virtual eroi operator/(const supporter&) const;

    virtual eroi operator+(const debuffer&) const;
    virtual eroi operator-(const debuffer&) const;
    virtual eroi operator*(const debuffer&) const;
    virtual eroi operator/(const debuffer&) const;

    string Supporto() const;
    int Eff_Supporto() const;
    string Debuff() const;
    int Eff_Debuff() const;
private:
    string supporto;
    int eff_supporto;
    string debuff;
    int eff_debuff;
};

ostream &operator<<(ostream& os, const eroi& c);

/*"gioco di carte" in cui l'addizione spiega chi sono i compagni, la sottrazione chi sono i nemici, la moltiplicazione
 * chi fa da supporto e la divisione chi fa da debuff. Ci sono vari tipi di carte, dall'eroe (Giovanna d'Arco) agli
 * uomini comuni (contadino). I parametri saranno robe come vitalià, energia, supporto, livello supporto, in quanto ci
 * saranno nove tipi di supporto (da 0 a 9), debuff e livello debuff (stesso discorso livello supporto).
 * ps: supertipo con meno informazioni */


#endif // CARTE_EROI_H
