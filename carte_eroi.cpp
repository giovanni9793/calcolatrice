#include "carte_eroi.h"



/*"gioco di carte" in cui l'addizione spiega chi sono i compagni, la sottrazione chi sono i nemici, la moltiplicazione
 * chi fa da supporto e la divisione chi fa da debuff. Ci sono vari tipi di carte, dall'eroe (Giovanna d'Arco) agli
 * uomini comuni (contadino). I parametri saranno robe come vitalià, energia, supporto, livello supporto, in quanto ci
 * saranno nove tipi di supporto (da 0 a 9). */

int plus2number(const string& s) {
    if (s=="nessuno") {
        return 0;
    }
    else if (s=="vita") {
        return 1;
    }
    else if (s=="energia") {
        return 2;
    }
    else {
        return 3;
    }
}

comuni comuni::operator=(const comuni & c)         //semplice assegnazione per avere nuovi oggetti con nomi diversi,
{                                                  //ma stesse caratteristiche
    vita=c.vita;
    energia=c.energia;
    return *this;
}

comuni comuni::operator+(const comuni & c) const   //definizione dei "compagni", le cui caratteristiche si sommano
{
    comuni t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

comuni comuni::operator-(const comuni & c) const   //definizione di "nemici", le cui caratteristiche vengono sottratte
{                                                  //alla prima unità
    comuni t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

comuni comuni::operator*(const supporter & s)  const  //definizione di "supporter", non ha senso nelle unità comuni,
{                                                     //definita per sollevare eccezioni
    comuni t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

comuni comuni::operator/(const supporter &) const
{
    //throw();
    return *this;
}

comuni comuni::operator*(const debuffer &) const
{
    //throw();
    return *this;
}

comuni comuni::operator/(const debuffer & s)  const    //definizione di "debuffer", non ha senso nelle unità comuni
{                                                      //definita per sollevare eccezioni

    comuni t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

comuni comuni::operator*(const eroi & s) const
{
    comuni t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

comuni comuni::operator/(const eroi & s) const
{
    comuni t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

string comuni::Nome() const
{
    return nome;
}

int comuni::Vita() const                            //metodo pubblico per conoscere il campo dati privato "vita"
{
    return vita;
}

int comuni::Energia() const                         //metodo pubblico per conoscere il campo dati privato "energia"
{
    return energia;
}

bool comuni::Usato() const
{
    return usato;
}

ostream &operator<<(ostream & os, const comuni& c)
{
    return os<<"----------------------------"<<endl
            <<"Caratteristiche"<< endl
            <<"Nome: "<<c.Nome()<<endl
            <<"Vita: " << c.Vita()<<endl
            <<"Energia: " << c.Energia() <<endl
            <<"----------------------------"<<endl;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

supporter supporter::operator=(const supporter & c)
{
    vita=c.vita;
    energia=c.energia;
    supporto=c.supporto;
    eff_supporto=c.eff_supporto;
    return *this;
}

supporter supporter::operator+(const supporter & c) const
{
    supporter t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator-(const supporter & c) const
{
    supporter t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator*(const supporter & s) const
{
    supporter t=*this;
    switch(plus2number(s.supporto)){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.eff_supporto);
        break;
    case 2:
        t.energia=t.energia+(3*s.eff_supporto);
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

supporter supporter::operator/(const supporter &) const
{
    //throw();
    return *this;
}

supporter supporter::operator+(const debuffer & c) const
{
    supporter t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator-(const debuffer & c) const
{
    supporter t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator*(const debuffer &) const
{
    //throw();
    return *this;
}

supporter supporter::operator/(const debuffer & s) const
{
    supporter t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

supporter supporter::operator+(const eroi & c) const
{
    supporter t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator-(const eroi & c) const
{
    supporter t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

supporter supporter::operator*(const eroi & s) const
{
    supporter t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

supporter supporter::operator/(const eroi & s) const
{
    supporter t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

string supporter::Supporto() const
{
    return supporto;
}

int supporter::Eff_Supporto() const
{
    return eff_supporto;
}

ostream &operator<<(ostream & os, const supporter& c)
{
    if(c.Usato()) {
        return os<<comuni(c);
    }
    else {
        return os<<"----------------------------"<<endl
                <<"Caratteristiche"<< endl
                <<"Nome: "<<c.Nome()<<endl
                <<"Vita: " << c.Vita()<<endl
                <<"Energia: " << c.Energia() <<endl
                <<"Supporto: " << c.Supporto()<<endl
                <<"Effetto Supporto: " << c.Eff_Supporto() <<endl
                <<"----------------------------"<<endl;
    }
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

debuffer debuffer::operator=(const debuffer & c)
{
    vita=c.vita;
    energia=c.energia;
    debuff=c.debuff;
    eff_debuff=c.eff_debuff;
    return *this;
}

debuffer debuffer::operator+(const debuffer & c) const
{
    debuffer t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    return t;
}

debuffer debuffer::operator-(const debuffer & c) const
{
    debuffer t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

debuffer debuffer::operator*(const debuffer &) const
{
    //throw();
    return *this;
}

debuffer debuffer::operator/(const debuffer & s) const
{
    debuffer t=*this;
    switch(plus2number(s.debuff)){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.eff_debuff);
        break;
    case 2:
        t.energia=t.energia-(3*s.eff_debuff);
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

debuffer debuffer::operator+(const supporter & c) const
{
    debuffer t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

debuffer debuffer::operator-(const supporter & c) const
{
    debuffer t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

debuffer debuffer::operator*(const supporter & s) const
{
    debuffer t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

debuffer debuffer::operator/(const supporter &) const
{
    //throw();
    return *this;
}

debuffer debuffer::operator+(const eroi & c) const
{
    debuffer t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

debuffer debuffer::operator-(const eroi & c) const
{
    debuffer t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

debuffer debuffer::operator*(const eroi & s) const
{
    debuffer t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

debuffer debuffer::operator/(const eroi & s) const
{
    debuffer t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

string debuffer::Debuff() const
{
    return debuff;
}

int debuffer::Eff_Debuff() const
{
    return eff_debuff;
}

ostream &operator<<(ostream & os, const debuffer& c)
{
    if(c.Usato()) {
        return os << comuni(c);
    }
    else {
        return os<<"----------------------------"<<endl
                <<"Caratteristiche"<< endl
                <<"Nome: "<<c.Nome()<<endl
                <<"Vita: " << c.Vita()<<endl
                <<"Energia: " << c.Energia() <<endl
                <<"Debuff: " << c.Debuff()<<endl
                <<"Effetto Debuff: " << c.Eff_Debuff()<<endl
                <<"----------------------------"<<endl;
    }
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

eroi eroi::operator=(const eroi & c)
{
    vita=c.vita;
    energia=c.energia;
    supporto=c.supporto;
    eff_supporto=c.eff_supporto;
    debuff=c.debuff;
    eff_debuff=c.eff_debuff;
    return *this;
}

eroi eroi::operator+(const eroi & c) const
{
    eroi t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator-(const eroi & c) const
{
    eroi t(*this);
    t.vita-=c.Vita();
    t.energia-=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator*(const eroi & s) const
{
    eroi t=*this;
    switch(plus2number(s.supporto)){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.eff_supporto);
        break;
    case 2:
        t.energia=t.energia+(3*s.eff_supporto);
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

eroi eroi::operator/(const eroi & s) const
{
    eroi t=*this;
    switch(plus2number(s.debuff)){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.eff_debuff);
        break;
    case 2:
        t.energia=t.energia-(3*s.eff_debuff);
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

eroi eroi::operator+(const supporter & c) const
{
    eroi t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator-(const supporter & c) const
{
    eroi t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator*(const supporter & s) const
{
    eroi t=*this;
    switch(plus2number(s.Supporto())){
    case 0:
        cout << "Sicuro si tratti di un supporter?";
        //throw();
        break;
    case 1:
        t.vita=t.vita+(3*s.Eff_Supporto());
        break;
    case 2:
        t.energia=t.energia+(3*s.Eff_Supporto());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

eroi eroi::operator/(const supporter &) const
{
    //throw();
    return *this;
}

eroi eroi::operator+(const debuffer & c) const
{
    eroi t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator-(const debuffer & c) const
{
    eroi t(*this);
    t.vita+=c.Vita();
    t.energia+=c.Energia();
    t.usato=true;
    return t;
}

eroi eroi::operator*(const debuffer &) const
{
    //throw();
    return *this;
}

eroi eroi::operator/(const debuffer & s) const
{
    eroi t=*this;
    switch(plus2number(s.Debuff())){
    case 0:
        cout << "Sicuro si tratti di un debuffer?";
        //throw();
        break;
    case 1:
        t.vita=t.vita-(3*s.Eff_Debuff());
        break;
    case 2:
        t.energia=t.energia-(3*s.Eff_Debuff());
        break;
    default:
        cout << "Qualcosa è andato storto :/";
        //throw();
    }
    t.usato=true;
    return t;
}

string eroi::Supporto() const
{
    return supporto;
}

int eroi::Eff_Supporto() const
{
    return eff_supporto;
}

string eroi::Debuff() const
{
    return debuff;
}

int eroi::Eff_Debuff() const
{
    return eff_debuff;
}

ostream &operator<<(ostream &os, const eroi &c)
{
    if(c.Usato()){
        return os<<comuni(c);
    }
    else {
        return os<<"----------------------------"<<endl
                <<"Caratteristiche"<< endl
                <<"Nome: "<<c.Nome()<<endl
                <<"Vita: " << c.Vita()<<endl
                <<"Energia: " << c.Energia() <<endl
                <<"Debuff: " << c.Debuff()<<endl
                <<"Effetto Debuff: " << c.Eff_Debuff()<<endl
                <<"Supporto: " << c.Supporto()<<endl
                <<"Effetto Supporto: " << c.Eff_Supporto() <<endl
                <<"----------------------------"<<endl;
    }
}
